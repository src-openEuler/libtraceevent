# libtraceevent

#### 介绍
Library to parse raw trace event formats

#### 软件架构
支持aarch64、x86_64 2种架构


#### 安装教程

1.  rpm -ivh libtraceevent***.rpm

#### 使用说明

1.  此软件仅提供动态库和静态库，没有二进制命令，使用方式参考man手册和libtraceevent-devel中的头文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
